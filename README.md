# README #

There are no special requirements to run this script. It will run either with Python 2 or Python 3. 

### What is this repository for? ###

* In a very rough way this script takes a player list from a txt file an generates ramdom teams with the number of players entered via command line. The results are shown on screen and written on a file.
* Players file: hofftastics.txt
* File for storing generated teams: hofftastic_games.txt
* version 0.1

### How do I get set up? ###

* Populate file hofftastics.txt with a player per line
* run hofftastics.py

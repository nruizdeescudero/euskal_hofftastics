import random
import sys

if sys.version_info[0] < 3:
    game_name = raw_input("Nombre del Juego:")
else:
    game_name = input("Nombre del Juego:")

hofftastics = []
hoff_file = open('hofftastics.txt', 'r')
hoffgames_file = open('hofftastic_games.txt', 'a')
mylist = hoff_file.read().splitlines()
members = len(mylist) + 1
display_line = 'Jugadores(' + str(len(mylist)) + ')'
print(display_line)
for line in mylist:
    hofftastics.append(line)
    print (line)
random.shuffle(hofftastics)

cont = 0    
while members > len(mylist):
    if cont > 0:
        print ('No puede haber m�s jugadores por equipo que los jugadores totales.')
    if sys.version_info[0] < 3:
        strmembers = raw_input("Num. jugadores por equipo: ")
    else:
        strmembers = input("Num. jugadores por equipo: ")
    members = int(strmembers)
    cont = cont + 1
    
header = '########## START OF ' + game_name + ' ##########\n'
players = 'JUGADORES: ' + str(len(hofftastics))
str_members = 'EQUIPOS DE ' + strmembers
lines_to_file = [str(header), players, str_members]

result = [hofftastics[i:i + members] for i in range(0, len(hofftastics), members)]
for item in result:
    lines_to_file.append(str(item))
footer = '\n########## END OF ' + game_name + ' ##########\n\n'
lines_to_file.append(str(footer))
with hoffgames_file as h_file:
    h_file.writelines(str(line) + '\n' for line in lines_to_file)
hoffgames_file.close()
for line in lines_to_file:
    print(str(line) + '\n')